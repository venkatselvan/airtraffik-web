const gulp = require('gulp');
const gutil = require('gulp-util');
const webpack = require('webpack');
const webpackStream = require('webpack-stream');
const WebpackDevServer = require('webpack-dev-server');
const webpackConfig = require('./webpack.config.js');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const eslint = require('gulp-eslint');
const surge = require('gulp-surge');

gulp.task('lint', () => {
  gulp.src(['./app/**/*.js', './app/**/*.jsx', '!node_modules/**'])
    .pipe(eslint())
    .pipe(eslint.format());
});

gulp.task('build', ['lint'], () => {
  let buildConfig = Object.create(webpackConfig);
  buildConfig.plugins.push(new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify('production')
  }));
  buildConfig.plugins.push(new UglifyJsPlugin());
  gulp.src('./app/index.jsx')
    .pipe(webpackStream(webpackConfig))
    .pipe(gulp.dest('dist'));
});


gulp.task('serve', ['lint'], () => {
  webpackConfig.entry.app.unshift('webpack-dev-server/client?http://localhost:8081/');
  // Start a webpack-dev-server
  let compiler = webpack(webpackConfig);

  new WebpackDevServer(compiler, {}).listen(8081, 'localhost', (err) => {
    if (err) throw new gutil.PluginError('dev', err);
    // Server listening
    gutil.log('[dev]', 'http://localhost:8081/webpack-dev-server/index.html');
  });
});

gulp.task('deploy', () => surge({
  project: 'dist/',
  domain: 'yttclient.surge.sh'
}));
