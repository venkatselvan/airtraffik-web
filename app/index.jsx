import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import 'semantic-ui-css/semantic.min.css';
import 'react-notifications/lib/notifications.css';
import 'react-table/react-table.css';
import getStore from './store/configure';
import App from './App.jsx';

let store = getStore();

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('App')
);
