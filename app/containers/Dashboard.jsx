import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  Container,
  Header as HTag,
  Table,
  Divider,
  Button,
  Label,
  Dropdown,
  Grid
} from 'semantic-ui-react';
import moment from 'moment';
import { keyBy, filter as _filter, each } from 'lodash'; 
import { Line, Bar } from 'react-chartjs';
import { DateInput } from 'semantic-ui-calendar-react';
import axios from 'axios';
import { NotificationManager } from 'react-notifications';
import CONSTANT from '../constants.js';
import Header from '../components/Header.jsx';

function convertMinsToHrsMins(mins) {
  let h = Math.floor(mins / 60);
  let m = mins % 60;
  h = h < 10 ? '0' + h : h;
  m = m < 10 ? '0' + m : m;
  return `${h}:${m}`;
}

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      slotAllocations: [],
      filteredSlotAllocations: [],
      inputs: {
        effectiveFrom: '',
        effectiveTo: ''
      },
      filter: {
        airline: 'all',
        allocated: 'all'
      }
    };
    this.onReload = this.onReload.bind(this);
    this.getChartData = this.getChartData.bind(this);
    this.onHandleChange = this.onHandleChange.bind(this);
    this.onHandleDateChange = this.onHandleDateChange.bind(this);
    this.onFilter = this.onFilter.bind(this);
    this.onAllocate = this.onAllocate.bind(this);
    this.onClearSlots = this.onClearSlots.bind(this);
  }

  componentDidMount() {
    this.onReload();
  }

  onReload() {
    axios.get(`${CONSTANT.SLOTALLOCATION_URL}/?airport=${this.props.airport}`).then((response) => {
      if (response.status === 200 && response.data.status === 'success') {
        this.setState({
          slotAllocations: response.data.slotAllocations,
          filteredSlotAllocations: response.data.slotAllocations,
          filter: {
            airline: 'all',
            allocated: 'all'
          }
        });
      }
    }).catch((err) => {
      NotificationManager.error(`Error loading slot allocations: ${err}`);
    });
  }

  onHandleDateChange(e, data) {
    let { name, value } = data;
    let { inputs } = this.state;
    inputs[name] = value;
    this.setState({ inputs });
  }

  onHandleChange(e, data) {
    let { name, value } = data;
    let { inputs } = this.state;
    inputs[name] = value;
    this.setState({ inputs });
  }

  onFilter({ filterBy, value }) {
    let { filter } = this.state;
    filter[filterBy] = value;
    this.setState({ filter }, () => {
      this.setState({ filteredSlotAllocations: this.getFilteredData() })
    });
  }

  // eslint-disable-next-line react/sort-comp
  getChartData(type) {
    const { slotAllocations } = this.state;
    if (type === 'departure') {
      const departures = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
      each(slotAllocations, (sa) => {
        // eslint-disable-next-line no-bitwise
        const x = ~~(sa.timeOfDept / 60);
        departures[x] += 1;
      });
      return departures;
    } else if (type === 'arrival') {
      const arrivals = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
      each(slotAllocations, (sa) => {
        // eslint-disable-next-line no-bitwise
        const x = ~~(sa.timeOfArrival / 60);
        arrivals[x] += 1;
      });
      return arrivals;
    } else if (type === 'otp') {
      const otps = [75.9, 75.3, 69.2, 64.0, 63.1, 63.1, 56.6, 33.5];
      return otps;
    }
  }

  // eslint-disable-next-line react/sort-comp
  getFilteredData() {
    let { filter, slotAllocations } = this.state;
    const filteredSlotAllocations = _filter(slotAllocations, (o) => {
      let passAllocationFilter = true;
      let passAirlineFilter = true;
      if (filter.allocated !== 'all' || filter.airline !== 'all') { 
        if (filter.allocated !== 'all') {
          const isAllocated = filter.allocated === 'allocated'? true: false;
          passAllocationFilter = o.allocated === isAllocated;
        }
        if (filter.airline !== 'all') {
          passAirlineFilter = o.airline === filter.airline;
        }
      }
      return passAirlineFilter && passAllocationFilter;
    });
    return filteredSlotAllocations;
  }

  onAllocate() {
    const { effectiveFrom, effectiveTo, allocationType } = this.state.inputs;
    axios.post(`${CONSTANT.SLOTALLOCATION_URL}/allocate/?airport=${this.props.airport}&effectiveFrom=${effectiveFrom}&effectiveTo=${effectiveTo}&allocationType=${allocationType}`).then((response) => {
      if (response.status === 200 && response.data.status === 'success') {
        this.onReload();
      } else {
        NotificationManager.error(`Error loading slot allocations: ${response.data.error}`);
      }
    }).catch((err) => {
      NotificationManager.error(`Error loading members: ${err}`);
    });
  }

  onClearSlots() {
    const effectiveFrom = this.state.inputs.effectiveFrom;
    const effectiveTo = this.state.inputs.effectiveTo;
    axios.delete(`${CONSTANT.SLOTALLOCATION_URL}/?airport=${this.props.airport}&effectiveFrom=${effectiveFrom}&effectiveTo=${effectiveTo}`).then((response) => {
      if (response.status === 200 && response.data.status === 'success') {
        this.onReload();
      } else {
        NotificationManager.error(`Error loading slot allocations: ${response.data.error}`);
      }
    }).catch((err) => {
      NotificationManager.error(`Error loading members: ${err}`);
    });
  }

  render() {
    const airlineById = keyBy(CONSTANT.AIRLINES, 'key');
    const daChartData = {
      labels: ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'],
      datasets: [
        {
          label: 'Departure',
          fillColor: 'rgba(82,61,204,0.2)',
          strokeColor: 'rgba(82,61,204,1)',
          pointColor: 'rgba(82,61,204,1)',
          pointStrokeColor: '#fff',
          pointHighlightFill: '#fff',
          pointHighlightStroke: 'rgba(82,61,204,1)',
          data: this.getChartData('departure')
        },
        {
          label: 'Arrival',
          fillColor: 'rgba(96,187,71,0.2)',
          strokeColor: 'rgba(96,187,71,1)',
          pointColor: 'rgba(96,187,71,1)',
          pointStrokeColor: '#fff',
          pointHighlightFill: '#fff',
          pointHighlightStroke: 'rgba(96,187,71,1)',
          data: this.getChartData('arrival')
        }
      ]
    };
    const otpChartData = {
      labels: ['GoAir', 'Vistara', 'SpiceJet', 'Indigo', 'Jet Airways', 'Jet Lite', 'Air India', 'Air Asia'],
      datasets: [
        {
          label: 'Airline OTPs',
          fillColor: 'rgba(82,61,204,0.2)',
          strokeColor: 'rgba(82,61,204,1)',
          pointColor: 'rgba(82,61,204,1)',
          pointStrokeColor: '#fff',
          pointHighlightFill: '#fff',
          pointHighlightStroke: 'rgba(82,61,204,1)',
          data: this.getChartData('otp')
        }
      ]
    };
    return (
      <Container
        fluid
      >
        <Header />
        <Container
          fluid
          style={{
            width: '90%',
            marginTop: '60px'
          }}
        >
          <HTag as="h1" style={{ textAlign: 'center', color: '#333', textTransform: 'uppercase' }}>Allocated Slots</HTag>
          <div className="search-container">
            <DateInput
              name="effectiveFrom"
              placeholder="Effective From"
              value={this.state.inputs.effectiveFrom}
              iconPosition="left"
              onChange={this.onHandleDateChange}
            />
            <DateInput
              name="effectiveTo"
              placeholder="Effective To"
              value={this.state.inputs.effectiveTo}
              iconPosition="left"
              onChange={this.onHandleDateChange}
            />
            <Dropdown
              name="allocationType"
              placeholder="Allocation Type"
              fluid
              selection
              options={CONSTANT.ALLOCATION_TYPES}
              onChange={this.onHandleChange}
              value={this.state.inputs.allocationType}
              style={{
                width: '280px',
                borderRadius: '30px'
              }}
            />
            <Button
              size="small"
              color="green"
              onClick={this.onAllocate}
            >
              Allocate Slot
            </Button>
            <Button
              size="small"
              color="black"
              onClick={this.onClearSlots}
            >
              Clear Slot
            </Button>
          </div>
          <div className="filter-container">
            <h3>Filter: </h3>
            <Dropdown
              name="airline"
              placeholder="Airlines"
              fluid
              selection
              options={[{ key: 'all', value: 'all', text: 'All Airlines' }, ...CONSTANT.AIRLINES]}
              onChange={(e, { value }) => { this.onFilter({ filterBy: 'airline', value }); }}
              value={this.state.filter.airline}
              style={{
                width: '280px',
                borderRadius: '30px'
              }}
            />
            <Dropdown
              name="allocated"
              placeholder="allocated"
              fluid
              selection
              options={[
                { key: 'all', value: 'all', text: 'All Status' },
                { key: 'allocated', value: 'allocated', text: 'Allocated' },
                { key: 'rejected', value: 'rejected', text: 'Rejected' }
              ]}
              onChange={(e, { value }) => { this.onFilter({ filterBy: 'allocated', value }); }}
              value={this.state.filter.allocated}
              style={{
                width: '200px',
                borderRadius: '30px'
              }}
            />
          </div>
          <Divider hidden clearing />
          <Table className="dash" selectable striped>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Airline</Table.HeaderCell>
                <Table.HeaderCell>Flight No</Table.HeaderCell>
                <Table.HeaderCell>Departure</Table.HeaderCell>
                <Table.HeaderCell>Departure Date</Table.HeaderCell>
                <Table.HeaderCell>Departure Time</Table.HeaderCell>
                <Table.HeaderCell>Departure Gate</Table.HeaderCell>
                <Table.HeaderCell>Arrival</Table.HeaderCell>
                <Table.HeaderCell>Arrival Date</Table.HeaderCell>
                <Table.HeaderCell>Arrival Time</Table.HeaderCell>
                <Table.HeaderCell>Arrival Gate</Table.HeaderCell>
                <Table.HeaderCell>Effective From</Table.HeaderCell>
                <Table.HeaderCell>Effective To</Table.HeaderCell>
                <Table.HeaderCell>Status</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {
                this.state.filteredSlotAllocations.map(sa =>
                  (
                    <Table.Row key={sa._id}>
                      <Table.Cell>
                        {airlineById[sa.airline].text}
                      </Table.Cell>
                      <Table.Cell>
                        {sa.flightNo}
                      </Table.Cell>
                      <Table.Cell>
                        {sa.departureFrom}
                      </Table.Cell>
                      <Table.Cell>
                        {moment(sa.dateOfDept).format('DD-MM-YY')}
                      </Table.Cell>
                      <Table.Cell>
                        {convertMinsToHrsMins(sa.timeOfDept)}
                      </Table.Cell>
                      <Table.Cell>
                        {
                          sa.departureGate >= 0 &&
                          `0${sa.departureGate}`
                        }
                      </Table.Cell>
                      <Table.Cell>
                        {sa.arrivalFrom}
                      </Table.Cell>
                      <Table.Cell>
                        {moment(sa.dateOfArrival).format('DD-MM-YY')}
                      </Table.Cell>
                      <Table.Cell>
                        {convertMinsToHrsMins(sa.timeOfArrival)}
                      </Table.Cell>
                      <Table.Cell>
                        {
                          sa.arrivalGate >= 0 &&
                          `0${sa.arrivalGate}`
                        }
                      </Table.Cell>
                      <Table.Cell>
                        {moment(sa.effectiveFrom).format('DD-MM-YY')}
                      </Table.Cell>
                      <Table.Cell>
                        {moment(sa.effectiveTo).format('DD-MM-YY')}
                      </Table.Cell>
                      <Table.Cell>
                        {
                          sa.allocated &&
                          <Label color="green">
                            <i className="fas fa-check" />
                          </Label>
                        }
                        {
                          !sa.allocated &&
                          <Label color="red">
                            <i className="fas fa-times" />
                          </Label>
                        }
                      </Table.Cell>
                    </Table.Row>
                 ))
            }
            </Table.Body>
          </Table>
          {
            this.state.filteredSlotAllocations.length === 0 &&
            <HTag style={{ textAlign: 'center', color: '#ddd' }} >
              <i className="fas fa-inbox" /> &nbsp; No Data
            </HTag>
          }
          <Grid divided="vertically">
            <Grid.Row columns={2} className="graph-container">
              <Grid.Column className="graph-grid">
                <h3>Departure vs Arrival</h3>
                <Line data={daChartData} width={550} />
              </Grid.Column>
              <Grid.Column className="graph-grid">
                <h3>Airline OTPs</h3>
                <Bar data={otpChartData} width={550} />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Container>
      </Container>
    );
  }
}

Dashboard.propTypes = {
  airport: PropTypes.string.isRequired
};

const mapStateToProps = state => ({
  airport: state.auth.airport
});

export default connect(mapStateToProps)(Dashboard);
