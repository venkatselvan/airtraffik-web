import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  Container,
  Header as HTag,
  Table,
  Divider,
  Button,
  Popup,
  Grid
} from 'semantic-ui-react';
import { DateInput } from 'semantic-ui-calendar-react';
import moment from 'moment';
import { keyBy, each } from 'lodash';
import axios from 'axios';
import { NotificationManager } from 'react-notifications';
import CONSTANT from '../constants.js';
import Header from '../components/Header.jsx';

function convertMinsToHrsMins(mins) {
  let h = Math.floor(mins / 60);
  let m = mins % 60;
  h = h < 10 ? '0' + h : h;
  m = m < 10 ? '0' + m : m;
  return `${h}:${m}`;
}

function getFrequencies(fr) {
  let frequency = '';
  const frequencies = {
    0: 'Sunday',
    1: 'Monday',
    2: 'Tuesday',
    3: 'Wednesday',
    4: 'Thursday',
    5: 'Friday',
    6: 'Saturday'
  };
  each(fr, (f) => {
    frequency = `${frequency}, ${frequencies[f]}`;
  });
  return frequency.substring(1, frequency.length);
}

class SlotRequests extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      slotAllocations: [],
      inputs: {
        effectiveFrom: '',
        effectiveTo: ''
      }
    };
    this.onReload = this.onReload.bind(this);
    this.onHandleDateChange = this.onHandleDateChange.bind(this);
  }

  componentDidMount() {
    this.onReload();
  }

  onReload() {
    axios.get(`${CONSTANT.SLOTREQUEST_URL}/?airport=${this.props.airport}`).then((response) => {
      if (response.status === 200 && response.data.status === 'success') {
        this.setState({ slotAllocations: response.data.slotRequest });
      }
    }).catch((err) => {
      NotificationManager.error(`Error loading slot allocations: ${err}`);
    });
  }

  onHandleDateChange(e, data) {
    let { name, value } = data;
    let { inputs } = this.state;
    inputs[name] = value;
    this.setState({ inputs });
  }

  onDeleteRequest(_id) {
    axios.delete(`${CONSTANT.SLOTREQUEST_URL}/${_id}`).then((response) => {
      if (response.status === 200 && response.data.status === 'success') {
        this.onReload();
      } else {
        NotificationManager.error(`Error deleting slot request: ${response.data.error}`);
      }
    }).catch((err) => {
      NotificationManager.error(`Error deleting slot request: ${err}`);
    });
  }

  onClearSlots() {
    const effectiveFrom = this.state.inputs.effectiveFrom;
    const effectiveTo = this.state.inputs.effectiveTo;
    axios.delete(`${CONSTANT.SLOTREQUEST_URL}/?airport=${this.props.airport}&effectiveFrom=${effectiveFrom}&effectiveTo=${effectiveTo}`).then((response) => {
      if (response.status === 200 && response.data.status === 'success') {
        this.onReload();
      } else {
        NotificationManager.error(`Error deleting slot requests: ${response.data.error}`);
      }
    }).catch((err) => {
      NotificationManager.error(`Error deleting slot requests: ${err}`);
    });
  }

  render() {
    const airlineById = keyBy(CONSTANT.AIRLINES, 'key');
    return (
      <Container
        fluid
      >
        <Header />
        <Container
          fluid
          style={{
            width: '90%',
            marginTop: '60px'
          }}
        >
          <HTag as="h1" style={{ textAlign: 'center', color: '#333', textTransform: 'uppercase' }}>Requested Slots</HTag>
          <div className="search-container" style={{ minWidth: '500px', width: '45%' }}>
            <DateInput
              name="effectiveFrom"
              placeholder="Effective From"
              value={this.state.inputs.effectiveFrom}
              iconPosition="left"
              onChange={this.onHandleDateChange}
            />
            <DateInput
              name="effectiveTo"
              placeholder="Effective To"
              value={this.state.inputs.effectiveTo}
              iconPosition="left"
              onChange={this.onHandleDateChange}
            />
            <Button
              size="small"
              color="black"
              onClick={this.onClearRequests}
            >
              Clear Requests
            </Button>
          </div>
          <Divider hidden clearing />
          <Table className="dash" selectable striped>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Airline</Table.HeaderCell>
                <Table.HeaderCell>Flight No</Table.HeaderCell>
                <Table.HeaderCell>Departure</Table.HeaderCell>
                <Table.HeaderCell>Departure Time</Table.HeaderCell>
                <Table.HeaderCell>Arrival</Table.HeaderCell>
                <Table.HeaderCell>Arrival Time</Table.HeaderCell>
                <Table.HeaderCell>Frequency</Table.HeaderCell>
                <Table.HeaderCell>Effective From</Table.HeaderCell>
                <Table.HeaderCell>Effective To</Table.HeaderCell>
                <Table.HeaderCell>Action</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {
                this.state.slotAllocations.map(sa =>
                  (
                    <Table.Row key={sa._id}>
                      <Table.Cell>
                        {airlineById[sa.airline].text}
                      </Table.Cell>
                      <Table.Cell>
                        {sa.flightNo}
                      </Table.Cell>
                      <Table.Cell>
                        {sa.departureFrom}
                      </Table.Cell>
                      <Table.Cell>
                        {convertMinsToHrsMins(sa.timeOfDept)}
                      </Table.Cell>
                      <Table.Cell>
                        {sa.arrivalFrom}
                      </Table.Cell>
                      <Table.Cell>
                        {convertMinsToHrsMins(sa.timeOfArrival)}
                      </Table.Cell>
                      <Table.Cell>
                        {getFrequencies(sa.frequency)}
                      </Table.Cell>
                      <Table.Cell>
                        {moment(sa.effectiveFrom).format('DD-MM-YY')}
                      </Table.Cell>
                      <Table.Cell>
                        {moment(sa.effectiveTo).format('DD-MM-YY')}
                      </Table.Cell>
                      <Table.Cell>
                        <Popup
                          trigger={<Button color="red"><i class="fas fa-trash"></i></Button>}
                          flowing
                          on="click"
                        >
                          <Grid centered divided columns={1}>
                            <Grid.Column textAlign='center'>
                              <p>
                                <b>Are you sure?</b>
                              </p>
                              <Button color="red" onClick={() => { this.onDeleteRequest(sa._id); }}>Delete Request</Button>
                            </Grid.Column>
                          </Grid>
                        </Popup>
                      </Table.Cell>
                    </Table.Row>
                 ))
            }
            </Table.Body>
          </Table>
        </Container>
      </Container>
    );
  }
}

SlotRequests.propTypes = {
  airport: PropTypes.string.isRequired
};

const mapStateToProps = state => ({
  airport: state.auth.airport
});

export default connect(mapStateToProps)(SlotRequests);
