import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  Container,
  Form,
  Header as HTag,
  Input,
  Button,
  Grid,
  Dropdown
} from 'semantic-ui-react';
import { DateInput, TimeInput } from 'semantic-ui-calendar-react';
import { has } from 'lodash';
import axios from 'axios';
import { NotificationManager } from 'react-notifications';
import CONSTANT from '../constants.js';
import Header from '../components/Header.jsx';

class RequestSlot extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      slot: {
        airline: '',
        flightNo: '',
        operatorCode: '',
        aircraftType: '',
        frequency: '',
        timeOfDept: '',
        timeOfArrival: '',
        departureFrom: '',
        arrivalFrom: '',
        effectiveFrom: '',
        effectiveTo: '',
        gate: ''
      },
      loading: false
    };

    this.onHandleChange = this.onHandleChange.bind(this);
    this.onHandleDateChange = this.onHandleDateChange.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
    this.onSlotUpdate = this.onSlotUpdate.bind(this);
  }

  componentDidMount() {
  }

  onInputChange(e, data) {
    let { name, value } = data;
    let { slot } = this.state;
    slot[name] = value;
    this.setState({ slot });
  }

  onHandleChange(e, data) {
    let { name, value } = data;
    let { slot } = this.state;
    slot[name] = value;
    this.setState({ slot });
  }

  onHandleDateChange(e, data) {
    let { name, value } = data;
    let { slot } = this.state;
    slot[name] = value;
    this.setState({ slot });
  }

  getMinutes(hm) {
    let a = hm.split(':');
    let minutes = (+a[0]) * 60 + (+a[1]);
    return minutes;
  }

  onSlotUpdate() {
    this.setState({ loading: true });
    let { slot } = this.state;
    axios.post(`${CONSTANT.SLOTREQUEST_URL}/`, {
      airline: slot.airline,
      flightNo: slot.flightNo,
      operatorCode: slot.operatorCode,
      aircraftType: slot.aircraftType,
      frequency: slot.frequency,
      effectiveFrom: moment(slot.effectiveFrom, 'DD-MM-YYYY').toDate(),
      effectiveTo: moment(slot.effectiveTo, 'DD-MM-YYYY').toDate(),
      timeOfDept: this.getMinutes(slot.timeOfDept),
      timeOfArrival: this.getMinutes(slot.timeOfArrival),
      departureFrom: slot.departureFrom,
      arrivalFrom: slot.arrivalFrom
    }).then((response) => {
      if (response.status === 200 && response.data.status === 'success') {
        this.setState({
          loading: false,
          slot: {
            airline: '',
            flightNo: '',
            operatorCode: '',
            aircraftType: '',
            frequency: '',
            effectiveFrom: '',
            effectiveTo: '',
            timeOfDept: '',
            timeOfArrival: '',
            departureFrom: '',
            arrivalFrom: '',
            gate: ''
          }
        });
        NotificationManager.info('Slot Requested Successfully!');
      } else if (has(response, 'data')) {
        this.setState({ loading: false });
        NotificationManager.error(`Error requesting slot: ${response.data.error}`);
      }
    }).catch((err) => {
      NotificationManager.error(`Error requesting slot: ${err}`);
    });
  }

  render() {
    return (
      <Container
        fluid
      >
        <Header />
        <Container
          fluid
          style={{
            width: '90%',
            marginTop: '60px',
            paddingBottom: '60px'
          }}
        >
          <HTag as="h1" style={{ textAlign: 'center', color: '#333', textTransform: 'uppercase' }}>Request New Slot</HTag>
          <Grid columns={1} stackable>
            <Grid.Row>
              <Grid.Column>
                <Form>
                  <h1 class="form-heading"><i class="fas fa-plane"></i> Flight Information</h1>
                  <Form.Field>
                    <Dropdown
                      name="airline"
                      placeholder="Airline"
                      fluid
                      selection
                      options={CONSTANT.AIRLINES}
                      onChange={this.onHandleChange}
                      value={this.state.slot.airline}
                    />
                  </Form.Field>
                  <Form.Field>
                    <Input
                      name="flightNo"
                      fluid
                      placeholder="Flight No"
                      type="text"
                      value={this.state.slot.flightNo}
                      onChange={this.onInputChange}
                    />
                  </Form.Field>
                  <Form.Field>
                    <Input
                      name="operatorCode"
                      fluid
                      placeholder="Operator Code"
                      type="text"
                      value={this.state.slot.operatorCode}
                      onChange={this.onInputChange}
                    />
                  </Form.Field>
                  <Form.Field>
                    <Dropdown
                      name="aircraftType"
                      placeholder="Aircraft Type"
                      fluid
                      selection
                      options={CONSTANT.AIRCRAFT_TYPES}
                      onChange={this.onHandleChange}
                      value={this.state.slot.aircraftType}
                    />
                  </Form.Field>
                  <Form.Field>
                    <Dropdown
                      name="frequency"
                      placeholder="Frequency"
                      multiple
                      fluid
                      selection
                      options={CONSTANT.FREQUENCY}
                      onChange={this.onHandleChange}
                      value={this.state.slot.frequency}
                    />
                  </Form.Field>
                  <h1 class="form-heading"><i class="fas fa-plane-departure"></i> Departure</h1>
                  <Form.Group widths="equal">
                    <Form.Field>
                      <Dropdown
                        name="departureFrom"
                        placeholder="Departure From"
                        fluid
                        selection
                        options={CONSTANT.AIRPORTS}
                        onChange={this.onHandleChange}
                        value={this.state.slot.departureFrom}
                      />
                    </Form.Field>
                    <Form.Field>
                      <TimeInput
                        name="timeOfDept"
                        placeholder="Departure Time"
                        value={this.state.slot.timeOfDept}
                        iconPosition="left"
                        onChange={this.onHandleDateChange}
                      />
                    </Form.Field>
                  </Form.Group>
                  <h1 class="form-heading"><i class="fas fa-plane-arrival"></i> Arrival</h1>
                  <Form.Group widths="equal">
                    <Form.Field>
                      <Dropdown
                        name="arrivalFrom"
                        placeholder="Arrival From"
                        fluid
                        selection
                        options={CONSTANT.AIRPORTS}
                        onChange={this.onHandleChange}
                        value={this.state.slot.arrivalFrom}
                      />
                    </Form.Field>
                    <Form.Field>
                      <TimeInput
                        name="timeOfArrival"
                        placeholder="Arrival Time"
                        value={this.state.slot.timeOfArrival}
                        iconPosition="left"
                        onChange={this.onHandleDateChange}
                      />
                    </Form.Field>
                  </Form.Group>
                  <h1 class="form-heading"><i class="far fa-clock"></i> Timeline Information</h1>
                  <Form.Group widths="equal">
                    <Form.Field>
                      <DateInput
                        name="effectiveFrom"
                        placeholder="Effective From"
                        value={this.state.slot.effectiveFrom}
                        iconPosition="left"
                        onChange={this.onHandleDateChange}
                      />
                    </Form.Field>
                    <Form.Field>
                      <DateInput
                        name="effectiveTo"
                        placeholder="Effective To"
                        value={this.state.slot.effectiveTo}
                        iconPosition="left"
                        onChange={this.onHandleDateChange}
                      />
                    </Form.Field>
                  </Form.Group>
                  <Form.Field>
                    <Button
                      color="green"
                      content="Request Slot"
                      floated="right"
                      onClick={this.onSlotUpdate}
                      loading={this.state.loading}
                    />
                  </Form.Field>
                </Form>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Container>
      </Container>
    );
  }
}

const mapStateToProps = () => ({});

export default withRouter(connect(mapStateToProps)(RequestSlot));
