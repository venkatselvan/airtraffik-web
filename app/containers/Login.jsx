import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Form, Button, Message, Header, Dropdown } from 'semantic-ui-react';
import { isEmpty } from 'lodash';
import CONSTANT from '../constants.js';
import { setAuth } from '../store/actions/auth.jsx';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      airport: null,
      message: '',
      error: false,
      loading: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.performAuth = this.performAuth.bind(this);
  }

  handleChange(e, { value }) {
    this.setState({ airport: value });
  }
  
  performAuth(e) {
    e.preventDefault();
    this.setState({ loading: true });
    let { airport } = this.state;
    if (isEmpty(airport)) {
      let message = 'Please select an airport';
      this.setState({ error: true, message, loading: false });
      return;
    }
    this.props.setAuth(airport);
  }

  render() {
    return (
      <Form className="login-form"
      >
        <Header as="h1" className="logo-header" style={{ textAlign: 'center'}}>
          <i class="fas fa-plane"></i>AirTrafik
        </Header>
        <Form.Field>
          <Dropdown
            name="airport"
            placeholder="Airport"
            fluid
            selection
            options={CONSTANT.AIRPORTS}
            onChange={this.handleChange}
            value={this.state.airport}
          />
        </Form.Field>
        <Button
          size="large"
          color="green"
          type="submit"
          fluid
          onClick={this.performAuth}
          loading={this.state.loading}
        >
          Login
        </Button><br></br>
        <Message
          positive={!this.state.error}
          negative={this.state.error}
          hidden={isEmpty(this.state.message)}
        >
          { this.state.message }
        </Message>
      </Form>
    );
  }
}

Login.propTypes = {
  setAuth: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

const mapDispatchToProps = dispatch => ({
  setAuth: (airport) => {
    dispatch(setAuth(airport));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
