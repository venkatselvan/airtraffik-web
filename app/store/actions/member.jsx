const loadMembers = products => ({
  type: 'LOAD_MEMBERS',
  products
});

export { loadMembers };
