const setAuth = (airport) => ({
  type: 'SET_AUTH',
  airport
});

export { setAuth };
