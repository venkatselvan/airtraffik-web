const appReducer = (state = { loading: false }, action) => {
  switch (action.type) {
    case 'SET_LOADING_STATE':
      return { loading: action.loading };
    default: return state;
  }
};

const authReducer = (state = {
  airport: null
}, action) => {
  switch (action.type) {
    case 'SET_AUTH':
      return {
        airport: action.airport
      };
    case 'LOAD_STORED_STATE':
      return action.storedState;
    default: return state;
  }
};

const memberReducer = (state = { products: [] }, action) => {
  switch (action.type) {
    case 'LOAD_MEMBERS':
      return { members: action.members };
    default: return state;
  }
};

export default {
  appReducer,
  authReducer,
  memberReducer
};
