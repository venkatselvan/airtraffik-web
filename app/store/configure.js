import { combineReducers, createStore, applyMiddleware } from 'redux';
import { createSession } from 'redux-session';
import reducers from './reducers.js';

const configure = () => {
  let reducer = combineReducers({
    app: reducers.appReducer,
    auth: reducers.authReducer,
    member: reducers.memberReducer
  });

  const session = createSession({
    ns: 'airtrafik',
    selectState: state => ({
      airport: state.auth.airport,
    })
  });

  let store = createStore(reducer, applyMiddleware(session));

  return store;
};

export default configure;
