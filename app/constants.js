const DOMAIN = 'http://localhost:3000';
const SLOTREQUEST_URL = `${DOMAIN}/slotrequest`;
const SLOTALLOCATION_URL = `${DOMAIN}/slotallocation`;
const AIRPORTS = [
  { key: 'BOM', text: 'Chhatrapati Shivaji International Airport', value: 'BOM' },
  { key: 'MAA', text: 'Chennai International Airport', value: 'MAA' },
  { key: 'CCU', text: 'Netaji Subhas Chandra Bose International Airport', value: 'CCU' },
  { key: 'IGI', text: 'Indira Gandhi International Airport', value: 'IGI' }
];

const AIRCRAFT_TYPES = [
  {
    key: 'atr', value: 'atr', text: 'ATR'
  },
  {
    key: 'a-320', value: 'a-320', text: 'Airbus 320'
  },
  {
    key: 'b-737', value: 'b-737', text: 'Boeing 737'
  }
];

const AIRLINES = [
  {
    key: 'go-air', value: 'go-air', text: 'Go Air'
  },
  {
    key: 'vistara', value: 'vistara', text: 'Vistara'
  },
  {
    key: 'spicejet', value: 'spicejet', text: 'SpiceJet'
  },
  {
    key: 'indigo', value: 'indigo', text: 'Indigo'
  },
  {
    key: 'jet-airways', value: 'jet-airways', text: 'Jet Airways'
  },
  {
    key: 'jet-lite', value: 'jet-lite', text: 'Jet Lite'
  },
  {
    key: 'air-india', value: 'air-india', text: 'Air India'
  }
];

const FREQUENCY = [
  { key: '0', value: 0, text: 'Sunday' },
  { key: '1', value: 1, text: 'Monday' },
  { key: '2', value: 2, text: 'Tuesday' },
  { key: '3', value: 3, text: 'Wednesday' },
  { key: '4', value: 4, text: 'Thursday' },
  { key: '5', value: 5, text: 'Friday' },
  { key: '6', value: 6, text: 'Saturday' }
];

const ALLOCATION_TYPES = [
  { key: 'DAY_WISE', value: 'DAY_WISE', text: 'Day based allocation' },
  { key: 'REQUEST_WISE', value: 'REQUEST_WISE', text: 'Request based allocation' }
];

export default {
  DOMAIN,
  SLOTREQUEST_URL,
  SLOTALLOCATION_URL,
  AIRLINES,
  AIRPORTS,
  AIRCRAFT_TYPES,
  FREQUENCY,
  ALLOCATION_TYPES
};
