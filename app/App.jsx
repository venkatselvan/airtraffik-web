import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Container } from 'semantic-ui-react';
import axios from 'axios';
import { HashRouter, Route, Redirect } from 'react-router-dom';
import { NotificationContainer } from 'react-notifications';
import Login from './containers/Login.jsx';
import Dashboard from './containers/Dashboard.jsx';
import { setPayment } from './store/actions/auth.jsx';
import RequestSlot from './containers/RequestSlot.jsx';
import SlotRequests from './containers/SlotRequests.jsx';

class AppComponent extends React.Component {
  constructor(props) {
    super(props);
    this.isLoggedIn = this.isLoggedIn.bind(this);
  }

  componentWillMount() {
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.auth.token !== nextProps.auth.token) {
      axios.defaults.headers.common.Authorization = nextProps.auth.token;
    }
  }

  isLoggedIn() {
    let { auth } = this.props;
    if (!auth.airport) {
      return false;
    }
    return true;
  }

  render() {
    return (
      <HashRouter>
        <Container
          fluid
          style={{
            paddingBottom: '60px'
          }}
        >
          <Route
            exact
            path="/"
            render={() => {
              if (!this.isLoggedIn()) {
                return (<Redirect to="/login" />);
              }
              return (<Dashboard />);
            }}
          />
          <Route
            exact
            path="/login"
            render={() => (
              this.isLoggedIn() ? (
                <Redirect to="/" />
              ) : (
                <Login />
              )
            )}
          />
          <Route
            exact
            path="/requests"
            render={(props) => {
              if (!this.isLoggedIn()) {
                return (<Redirect to="/login" />);
              }
              return (<SlotRequests />);
            }}
          />
          <Route
            exact
            path="/request"
            render={(props) => {
              if (!this.isLoggedIn()) {
                return (<Redirect to="/login" />);
              }
              return (<RequestSlot />);
            }}
          />
          <Route
            exact
            path="/team/:id"
            render={(props) => {
              if (!this.isLoggedIn()) {
                return (<Redirect to="/login" />);
              } else if (this.isAdmin()) {
                return (<TeamDetails teamId={props.match.params.id} />);
              }
            }}
          />
          <NotificationContainer />
        </Container>
      </HashRouter>
    );
  }
}

AppComponent.propTypes = {
  auth: PropTypes.shape({
    token: PropTypes.string,
    validity: PropTypes.string,
    shopId: PropTypes.string
  }).isRequired,
  setPayment: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

const mapDispatchToProps = dispatch => ({
  setPayment: (status) => {
    dispatch(setPayment(status));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(AppComponent);
