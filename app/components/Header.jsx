import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Menu, Image } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { setAuth } from '../store/actions/auth.jsx';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
    this.onLogout = this.onLogout.bind(this);
    this.isAdmin = this.isAdmin.bind(this);
    this.isModerator = this.isModerator.bind(this);
  }

  onLogout(e) {
    e.preventDefault();
    this.props.setAuth(null);
  }

  isAdmin() {
    let { auth } = this.props;
    return auth.isAdmin;
  }

  isModerator() {
    let { auth } = this.props;
    return auth.isModerator;
  }

  render() {
    return (
      <Menu
        secondary
        style={{
          backgroundColor: '#523dcc',
          boxShadow: '0px 0px 30px rgba(0,0,0,0.1)',
          padding: '20px 30px'
        }}
      >
        <Menu.Item className="logo">
          <Link to="/">
            <h2><i className="fas fa-plane"></i>AirTrafik</h2>
          </Link>
        </Menu.Item>
        <Menu.Item className="airport-name">
          <Link to="/">
            {this.props.auth.airport}
          </Link>
        </Menu.Item>
        <Menu.Item>
          <Link to="/requests">
            All Requests
          </Link>
        </Menu.Item>
        <Menu.Item>
          <Link to="/request">
            Request New Slot
          </Link>
        </Menu.Item>
        <Menu.Menu position="right">
          <Menu.Item>
            <a href="#" onClick={this.onLogout}>Switch Airport</a>
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    );
  }
}

Header.propTypes = {
  setAuth: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};


const mapStateToProps = state => ({
  auth: state.auth
});

const mapDispatchToProps = dispatch => ({
  setAuth: (airport) => {
    dispatch(setAuth(airport));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
